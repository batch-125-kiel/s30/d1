const express= require('express');
const mongoose = require('mongoose');


const app = express();

app.use(express.json());
//app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://kielpaulo:damienzk@cluster0.y6fy9.mongodb.net/b125-tasks?retryWrites=true&w=majority")


.then(()=>{
	console.log('Successfully connected to the database');


})

.catch((error)=>{

	console.log(error);
});

app.listen(3000, ()=>console.log('Server is running'));//with a callback function to display message



const userSchema = new mongoose.Schema({

	firstName: String,
	lastName: String,
	userName: String,
	password: String

});

UserModel = mongoose.model('User', userSchema);
//console.log(typeof userModel);
//"612e2efbf45e9627e86636d0"
//console.log(userSchema);

app.post('/add-user',(req, res)=>{

	let newUser = new UserModel(req.body);

	newUser.save((error, savedUser)=>{

		if(error){

			console.log(error);


		}else{

			res.send(`New user added ${newUser}`);

		}


	})

});

app.get('/retrieve-users', (req, res)=>{

	UserModel.find({},(error, retrievedUsers)=>{

		if(error){

			console.log(error);

		}else{

			res.send(retrievedUsers);
		}
	})

});

app.put('/edit-user/:userId', (req, res)=>{

	let userId = req.params.userId;

	UserModel.findByIdAndUpdate(userId,{password: "newpassword"},(error, updatedUser)=>{

		if(error){

			console.log(error);
		}else{

			res.send(`User's password succesfully updated`);
		}


	})

});

app.delete('/delete-user/:userId',(req, res)=>{

	let userId = req.params.userId;
	UserModel.findByIdAndDelete(userId,(error, deletedUser)=>{

		if(error){

			res.send(error);
		}else{

			res.send(`${userId} has been succesfully deleted`);
		}

	});



})

