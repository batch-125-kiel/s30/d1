const express = require('express');

//this code os to be uesed on our db connection and the createion of our schema and model for our existing mongoDB atlas connection
const mongoose =  require('mongoose');

const app = express(); // creating an app server through the use of app variable

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//mongoose.connect is a way to connect our mongoDB atlas db to our server.

mongoose.connect("mongodb+srv://kielpaulo:damienzk@cluster0.y6fy9.mongodb.net/b125-tasks?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
		// Due to updates made by mongoDB developers, the default connection string is being logged as an error
		// to skip the error that we are going to encounter, we will use the newurlParser and UnifiedTopology objects inside our mongoose connect.

	}

).then(()=>{//if the mongoose suceeded on the connection, then we will log successfull message
	console.log('Connected to the Database');
}).catch((error)=>{//handles error when the mongoose failed to connect in our mongodb atlas database
	console.log(error);

})

/*----------------------------------------------------*/
// Schema gives a structure of what kind of record/document we are going to contain on our database

//schema() method, acts like a blueprint to our data
//we used the Schema() constructor of the mongoose dependency to create a new schema object for our tasks collection, new keyword creates new schema

const taskSchema = new mongoose.Schema({
	//define the fields with their corresponding data type
	//For task, it needs a filed called 'name' and 'status'
	//The field name has a data type of string
	//the field status has a data type of Boolean with a default value of false
	name: String,
	status: {

		type: Boolean,
		//default value are the predefined values for a filed if we don't put any value
		default: false
	}

});

/*Models: to perform CRUD for our defined collections with corresponding schema*/

//task variable here will contain the model for our tasks collection and should perform the CRUD operations
	//the first param of mongoose.model indicates the collection in where to store the data. Take note, the collection name must be written in singular form, sentence case, must be in uppercase the first letter
	// the second param is used to specify the schema/blueprint of the documents that will be stored on the tasks collection. 
const Task = mongoose.model('Task', taskSchema);



/*Business Logic - To do list application

-CRUD operation for our Tasks collection


*/

//insert new task
app.post('/add-task', (req, res)=>{

	//first, call the model for our task collection
	//create an instance of the task model and save it to our database
		//creating a new task here 'PM Break' through the Task model
	let newTask = new Task({
		name: req.body.name
		//req.body.name for postman body insertion

	});
	//telling our server that the newTask will now be saved as a new document to our collection in our database
		//.save(), saves a new document to our db
		//in our callback it will recieve 2 params, error and the saved document
		//saveTask shall contain the newly saved document from the db once the saving process is succesful.
	newTask.save((error, savedTask)=>{

		if(error){
			console.log(error);
		}else{

			res.send(`New task is succesfully saved!${savedTask}`)
		}

	});

});



/* Mini activity -  perform create operation for our users colection*/

const userSchema = new mongoose.Schema({

	firstName: String,
	lastName: String,
	userName: String,
	password: String

});

const User = mongoose.model('User', userSchema);


app.post('/register',(req, res)=>{

	let newUser = new User({

		firstName:req.body.firstName,
		lastName: req.body.lastName,
		userName: req.body.userName,
		password: req.body.password
		
	});

		newUser.save((error, savedUser)=>{

		if(error){
			console.log(error);
		}else{

			res.send(`New user is succesfully saved!${savedUser}`)
		}

	});



});

//RETRIEVE
app.get('/retrieve-tasks', (req, res)=>{

	//find will retrieve all the documents in the tasks collection
	//the error on the callback will handle the errors encountered while retrieve the records
	//the records on the call back will handle the raw data from the database
	Task.find({},(error, records)=>{

		if(error){

			console.log(error);

		}else{

			res.send(records);

		}


	});


});

//retrieve tasks that are done
app.get('/retrieve-tasks-done',(req, res)=>{
	
	//the task model will return all the tasks that are status true

	Task.find({status: true},(error, records)=>{

		if(error){

			console.log(error);
		}else{

			res.send(records);
		}

	});

});

//UPDATE operation
	//with wildcard URL
app.put('/complete-task/:taskId', (req,res)=>{

	//check url params
		//res.send({urlParams: req.params.taskId});

//1. find the specific record using its ID
//2. And update it

//findByIdAndUpdate()

let taskId = req.params.taskId;

//url parameters -  these are the values defined on the URLs
// to get the url params- req.params.<paramsName>
//:taskID is a way to indicate that we are going to receive a url parameter, called wild card
	//findByIdAndUpdate()

Task.findByIdAndUpdate(taskId,{status: true},(error, updatedTask)=>{

		if(error){

			console.log(error);

		}else{

			res.send(`Task completed succesfully!`);

		}
	});

});
//DELETE

app.delete('/delete-task/:taskId', (req, res)=>{

	
	let taskId = req.params.taskId;
	//findByIdAndDelete()
		//find by id and delete the data
	Task.findByIdAndDelete(taskId,(error, deletedTask)=>{

		if(error){

			console.log(error);

		}else{

			res.send('Task deleted!');
		}

	});

});






app.listen(3000, ()=>console.log('Server is running'));//with a callback function to display message
